We provide state of the art, caring treatment of ear, nose and throat diseases. Our care spans from sinus disease to head and neck cancer and from disorders of hearing and balance to pediatric issues including ear infections and tonsillitis.

Address : 550 Water Street, Suite A, Santa Cruz, CA 95065

Phone : 831-476-4414
